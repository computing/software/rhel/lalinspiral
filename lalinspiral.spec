%define nightly %{nil}
%define _sysconfdir %{_prefix}/etc
%define release 1.1
%define configure_opts %{nil}

%if "%{?nightly:%{nightly}}%{!?nightly:0}" == "%{nil}"
%undefine nightly
%endif

# -- metadata ---------------

Name: lalinspiral
Version: 4.0.4
Release: %{?nightly:0.%{nightly}}%{!?nightly:%{release}}%{?dist}
License: GPLv2+
Source0: https://software.igwn.org/lscsoft/source/lalsuite/%{name}-%{version}%{?nightly:-%{nightly}}.tar.xz
URL: https://wiki.ligo.org/Computing/LALSuite
Packager: Adam Mercer <adam.mercer@ligo.org>
Prefix: %{_prefix}

# -- build requirements -----

# C
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: gsl-devel
BuildRequires: help2man >= 1.37
BuildRequires: liblal-devel >= 7.4.0
BuildRequires: liblalframe-devel >= 3.0.0
BuildRequires: liblalmetaio-devel >= 4.0.0
BuildRequires: liblalsimulation-devel >= 5.3.0
BuildRequires: liblalburst-devel >= 2.0.0
BuildRequires: make
BuildRequires: pkgconfig >= 0.18.0

# swig
BuildRequires: swig >= 3.0.11

# python3x
BuildRequires: python-srpm-macros
BuildRequires: python3-rpm-macros
BuildRequires: epel-rpm-macros
BuildRequires: python%{python3_pkgversion}
BuildRequires: python%{python3_pkgversion}-devel
BuildRequires: python%{python3_pkgversion}-lal >= 7.4.0
BuildRequires: python%{python3_pkgversion}-lalframe >= 3.0.0
BuildRequires: python%{python3_pkgversion}-lalmetaio >= 4.0.0
BuildRequires: python%{python3_pkgversion}-lalsimulation >= 5.3.0
BuildRequires: python%{python3_pkgversion}-lalburst >= 2.0.0
BuildRequires: python%{python3_pkgversion}-ligo-lw
BuildRequires: python%{python3_pkgversion}-lscsoft-glue
BuildRequires: python%{python3_pkgversion}-numpy >= 1.7
BuildRequires: python%{python3_pkgversion}-pytest

# octave
BuildRequires: octave-devel
BuildRequires: lal-octave >= 7.4.0
BuildRequires: lalframe-octave >= 3.0.0
BuildRequires: lalmetaio-octave >= 4.0.0
BuildRequires: lalsimulation-octave >= 5.3.0
BuildRequires: lalburst-octave >= 2.0.0

# -- packages ---------------

# lalinspiral
Summary: LSC Algorithm Inspiral Library
Requires: lib%{name} = %{version}-%{release}
Requires: python%{python3_pkgversion}-%{name} = %{version}-%{release}
Requires: python%{python3_pkgversion}-ligo-segments
Obsoletes: python2-%{name} <= 2.0.2-1
%description
The LSC Algorithm Inspiral Library for gravitational wave data analysis.
This package provides the runtime tools.

# liblalinspiral
%package -n lib%{name}
Summary: LSC Algorithm Inspiral Library - library package
Requires: liblal >= 7.4.0
Requires: liblalburst >= 2.0.0
Requires: liblalframe >= 3.0.0
Requires: liblalmetaio >= 4.0.0
Requires: liblalsimulation >= 5.3.0
Conflicts: %{name} <= 2.0.0-1
%description -n lib%{name}
The LSC Algorithm Inspiral Library for gravitational wave data analysis.
This package contains the shared-object libraries needed to run applications
that use the LAL Inspiral library.

# liblalinspiral-devel
%package -n lib%{name}-devel
Summary: Files and documentation needed for compiling programs that use LAL Inspiral
Requires: lib%{name} = %{version}-%{release}
Requires: gsl-devel
Requires: liblal-devel >= 7.4.0
Requires: liblalburst-devel >= 2.0.0
Requires: liblalframe-devel >= 3.0.0
Requires: liblalmetaio-devel >= 4.0.0
Requires: liblalsimulation-devel >= 5.3.0
Provides: %{name}-devel = %{version}-%{release}
Obsoletes: %{name}-devel < 2.0.1-1
%description -n lib%{name}-devel
The LSC Algorithm Inspiral Library for gravitational wave data analysis.
This package contains files needed build applications that use the LAL Inspiral
library.

# python3x-lalinspiral
%package -n python%{python3_pkgversion}-%{name}
Summary: Python %{python3_version} bindings for LALInspiral
Requires: lib%{name} = %{version}-%{release}
Requires: python%{python3_pkgversion}
Requires: python%{python3_pkgversion}-lal >= 7.4.0
Requires: python%{python3_pkgversion}-lalframe >= 3.0.0
Requires: python%{python3_pkgversion}-lalmetaio >= 4.0.0
Requires: python%{python3_pkgversion}-lalsimulation >= 5.3.0
Requires: python%{python3_pkgversion}-lalburst >= 2.0.0
Requires: python%{python3_pkgversion}-ligo-lw
Requires: python%{python3_pkgversion}-lscsoft-glue
Requires: python%{python3_pkgversion}-numpy >= 1.7
Requires: python%{python3_pkgversion}-tqdm
Obsoletes: python2-%{name} < 2.0.3-1
%{?python_provide:%python_provide python%{python3_pkgversion}-%{name}}
%description -n python%{python3_pkgversion}-%{name}
This package provides the Python bindings for LALInspiral.

# lalinspiral-octave
%package octave
Summary: Octave bindings for LALInspiral
Requires: lib%{name} = %{version}-%{release}
Requires: octave
Requires: lal-octave >= 7.4.0
Requires: lalburst-octave >= 2.0.0
Requires: lalframe-octave >= 3.0.0
Requires: lalmetaio-octave >= 4.0.0
Requires: lalsimulation-octave >= 5.3.0
%description octave
This package provides the Octave bindings for LALInspiral.

# -- build stages -----------

%prep
%setup -q -n %{name}-%{version}%{?nightly:-%{nightly}}

%build
%configure %{configure_opts} --disable-gcc-flags --enable-swig PYTHON=%{__python3}
%{__make} %{?_smp_mflags} V=1

%check
%{__make} %{?_smp_mflags} V=1 VERBOSE=1 check

%install
%make_install
find $RPM_BUILD_ROOT%{_libdir} -name '*.la' -delete

%post -n lib%{name} -p /sbin/ldconfig

%postun -n lib%{name} -p /sbin/ldconfig

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}%{?nightly:-%{nightly}}

# -- files ------------------

%files -n lib%{name}
%defattr(-,root,root)
%doc README.md
%license COPYING
%{_libdir}/*.so.*

%files -n lib%{name}-devel
%defattr(-,root,root)
%doc README.md
%license COPYING
%{_includedir}/lal
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*

%files -n python%{python3_pkgversion}-%{name}
%defattr(-,root,root)
%doc README.md
%license COPYING
%{python3_sitearch}/*
%exclude %{python3_sitearch}/%{name}/*.a

%files octave
%defattr(-,root,root)
%doc README.md
%license COPYING
%{_prefix}/lib*/octave/*/site/oct/*/lalinspiral.oct*

%files
%defattr(-,root,root)
%doc README.md
%license COPYING
%{_bindir}/*
%{_mandir}/man1/*
%{_sysconfdir}/*

# -- changelog --------------

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Thu Jan 11 2024 Adam Mercer <adam.mercer@ligo.org> 4.0.4-1.1
- Replace python-glue dependency with python-lscsoft-glue

* Fri Oct 20 2023 Adam Mercer <adam.mercer@ligo.org> 4.0.4-1
- Update for 4.0.4

* Fri Oct 06 2023 Adam Mercer <adam.mercer@ligo.org> 4.0.3-1
- Update for 4.0.3

* Mon Apr 24 2023 Adam Mercer <adam.mercer@ligo.org> 4.0.2-1
- Update for 4.0.2

* Thu Apr 06 2023 Adam Mercer <adam.mercer@ligo.org> 4.0.1-1
- Update for 4.0.1

* Mon Feb 06 2023 Adam Mercer <adam.mercer@ligo.org> 4.0.0-1
- Update for 4.0.0

* Mon Sep 05 2022 Adam Mercer <adam.mercer@ligo.org> 3.0.2-1
- Update for 3.0.2

* Thu Aug 18 2022 Adam Mercer <adam.mercer@ligo.org> 3.0.1-1
- Update for 3.0.1

* Tue Aug 02 2022 Adam Mercer <adam.mercer@ligo.org> 3.0.0-1
- Update for 3.0.0

* Thu Mar 03 2022 Adam Mercer <adam.mercer@ligo.rog> 2.0.6-1
- Update for 2.0.6

* Wed Jan 12 2022 Adam Mercer <adam.mercer@ligo.org> 2.0.5-1
- Update for 2.0.5

* Fri Dec 03 2021 Adam Mercer <adam.mercer@ligo.org> 2.0.4-1
- Update for 2.0.4

* Mon May 17 2021 Adam Mercer <adam.mercer@ligo.org> 2.0.3-1
- Update for 2.0.3

* Fri Feb 05 2021 Adam Mercer <adam.mercer@ligo.org> 2.0.2-1
- Update for 2.0.2

* Mon Jan 11 2021 Adam Mercer <adam.mercer@ligo.org> 2.0.1-1
- Update for 2.0.1

* Wed Oct 28 2020 Adam Mercer <adam.mercer@ligo.org> 2.0.0-1
- Update for 2.0.0

* Mon Jun 08 2020 Adam Mercer <adam.mercer@ligo.org> 1.10.1-1
- Update for 1.10.1

* Mon Dec 09 2019 Adam Mercer <adam.mercer@ligo.org> 1.10.0-2
- Packaging updates

* Tue Nov 26 2019 Adam Mercer <adam.mercer@ligo.org> 1.10.0-1
- O3b release

* Thu May 23 2019 Adam Mercer <adam.mercer@ligo.org> 1.9.0-1
- O3 release

* Mon Feb 25 2019 Adam Mercer <adam.mercer@ligo.org> 1.8.1-1
- ER14 release

* Thu Sep 13 2018 Adam Mercer <adam.mercer@ligo.org> 1.8.0-1
- Pre O3 release

* Tue Feb 07 2017 Adam Mercer <adam.mercer@ligo.org> 1.7.7-1
- O2 release

* Mon Sep 26 2016 Adam Mercer <adam.mercer@ligo.org> 1.7.6-1
- ER10 release

* Thu Jun 23 2016 Adam Mercer <adam.mercer@ligo.org> 1.7.5-1
- ER9 release

* Fri Mar 25 2016 Adam Mercer <adam.mercer@ligo.org> 1.7.4-1
- Pre O2 packaging test release
